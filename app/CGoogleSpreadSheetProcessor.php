<?php

	use Google\Client;

	require dirname( __DIR__ ) . '/vendor/autoload.php';

	/**
	 * Class CGoogleSpreadSheetProcessor
	 */
	class CGoogleSpreadSheetProcessor {

		/**
		 * @var string
		 */
		private $m_strSpreadSheetId = '1vFOdml3sze5oXaEspLgMEw34TjCuwIdoRpvoMe7iH0o';
		/**
		 * @var string
		 */
		private $m_strSheetName = 'titanic';

		CONST MAX_CONCURRENT_PROCESSES = 25;
		/**
		 * @return \Google\Client
		 * @throws \Google\Exception
		 */
		private function loadClient() {
			$strConfig = dirname( __DIR__ ) . '/config/';
			$client = new Client();
			$client->setApplicationName( 'Google Sheets API PHP Quickstart' );
			$client->setScopes( Google_Service_Sheets::SPREADSHEETS_READONLY );
			$client->setAuthConfig( $strConfig . 'credentials.json' );
			$client->setAccessType( 'offline' );
			$client->setPrompt( 'select_account consent' );

			// Load previously authorized token from a file, if it exists.
			// The file token.json stores the user's access and refresh tokens, and is
			// created automatically when the authorization flow completes for the first
			// time.
			$tokenPath = $strConfig . 'token.json';
			if( file_exists( $tokenPath ) ) {
				$accessToken = json_decode( file_get_contents( $tokenPath ), true );
				$client->setAccessToken( $accessToken );
			}

			// If there is no previous token or it's expired.
			if( $client->isAccessTokenExpired() ) {
				// Refresh the token if possible, else fetch a new one.
				if( $client->getRefreshToken() ) {
					$client->fetchAccessTokenWithRefreshToken( $client->getRefreshToken() );
				} else {
					// Request authorization from the user.
					$authUrl = $client->createAuthUrl();
					printf( "Open the following link in your browser:\n%s\n", $authUrl );
					print 'Enter verification code: ';
					$authCode = trim( fgets( STDIN ) );

					// Exchange authorization code for an access token.
					$accessToken = $client->fetchAccessTokenWithAuthCode( $authCode );
					$client->setAccessToken( $accessToken );

					// Check to see if there was an error.
					if( array_key_exists( 'error', $accessToken ) ) {
						throw new Exception( join( ', ', $accessToken ) );
					}
				}
				// Save the token to a file.
				if( !file_exists( dirname( $tokenPath ) ) ) {
					mkdir( dirname( $tokenPath ), 0700, true );
				}
				file_put_contents( $tokenPath, json_encode( $client->getAccessToken() ) );
			}

			return $client;
		}

		/**
		 * @throws \Google\Exception
		 */
		public function process() {
			$objGoogleService = new Google_Service_Sheets( $this->loadClient() );
			$strRange         = 'A4:L4'; //Row:Ax:Lx | Range:Ax:L
			$objResponse      = $objGoogleService->spreadsheets_values->get( $this->m_strSpreadSheetId, $this->m_strSheetName . '!' . $strRange );

			foreach( ( array ) $objResponse->getValues() as $arrmixRow ) {
				$arrjsonRow[] = [
					"keyValue" => [
						"icon" => "DESCRIPTION",
						"topLabel" => $strRange,
						"content" => implode(',', $arrmixRow )
					]
				];
			}

			$arrstrData = [
				"cards" => [
					[
						"header"   => [
							"title"    => "NewRelic",
							"subtitle" => "Reporting",
							"imageUrl" => "https://www.key-performance.eu/content/uploads/2017/01/logo-newrelic.png"
						],
						"sections" =>[
							[
								"widgets" => $arrjsonRow
							],
							[
								"widgets" => [
									"buttons" => [
										"textButton" => [
											"text"    => "OPEN DASHBOARD",
											"onClick" => [
												"openLink" => [
													"url" => "https://insights.newrelic.com/accounts/845842/dashboards/1271116?duration=1800000&kiosk=true"
												]
											]
										]
									]
								]
							]
						]

					]
				]
			];
			$this->googleNotification( $arrstrData );
		}

		/**
		 * +     * Process data vai api service
		 * +     */
		private function googleNotification( $arrstrData ) {

			$arrmixdata = [
				[
					'uri'  => 'https://chat.googleapis.com/v1/spaces/AAAAAbur2WI/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=BQ2tLy_1vR-8exUvGMxRsAuaGPTQvYcxMWkM3r9hWig%3D',
					'data' => $arrstrData
				]
			];

			$client = new GuzzleHttp\Client( [
				'timeout'  => 15.0, // Seconds
				'delay'    => 10.0, // Milliseconds
				'verify'   => false,
				'headers'  => [
					'Content-Type'  => 'application/json; charset=utf-8'
				]
			] );

			$promises = ( function () use ( $client, $arrmixdata ) {
				foreach( $arrmixdata as $arrstrPostString ) {
					yield $client->requestAsync( 'POST', $arrstrPostString['uri'], [ 'body' => json_encode( $arrstrPostString['data'] ) ] );
				}
			} )();

			$promise = new \GuzzleHttp\Promise\EachPromise(
				$promises, [
				'concurrency' => self::MAX_CONCURRENT_PROCESSES,
				'fulfilled'   => function ( \Psr\Http\Message\ResponseInterface $response ) {
					var_dump( $response->getBody()->getContents() );
				},
				'rejected'    => function ( $reason, $index ) {
					var_dump( $reason->getMessage() );
				},
			] );

			$promise->promise()->wait();
		}
	}

	( new CGoogleSpreadSheetProcessor() )->process();